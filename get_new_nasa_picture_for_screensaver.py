#!/usr/bin/python

import re
import os
import sys
import datetime
from urllib import urlopen
from BeautifulSoup import BeautifulSoup

print "Getting NASA Astronomy Picture of the Day page..."
html_page = urlopen('http://apod.nasa.gov/apod/').read()
print "Got it.  Now parsing out the image link from inside it..."
bs = BeautifulSoup(html_page)
picture_url = bs.findAll('a')[1]._getAttrMap()['href']
picture_url = "http://apod.nasa.gov/apod/" + picture_url
print "Get the picture url (%s)" % picture_url
print "Downloading the picture itself..."
picture = urlopen(picture_url).read()
picture_filename = "/usr/share/pixmaps/backgrounds/cosmos/%s.jpg" % datetime.date.today().strftime("%y-%m-%d")
fo = open(picture_filename, "wb")
fo.write(picture)
fo.close()
print "Saved %s.  Enjoy!" % picture_filename
if len(sys.argv) > 1 and sys.argv[1] == "background":
    print "Updating the desktop background to be today's picture..."
    os.system("gconftool-2 --type string --set /desktop/gnome/background/picture_filename %s" % (picture_filename,))
    print "Done"
