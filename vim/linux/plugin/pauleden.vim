" Paul's settings
colorscheme darkblue
set enc=utf-8
set expandtab 
set tabstop=4 
set shiftwidth=4
set autoindent
set smartindent
set backspace=indent,eol,start 
set nobackup
if has("autocmd")
  " Drupal *.module files.
  augroup module
    autocmd BufRead *.module set filetype=php
  augroup END
  augroup inc
    autocmd BufRead *.inc set filetype=php
  augroup END
endif

"Turn on Omni completion for certain file types
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType php set makeprg=php\ -l\ %
autocmd FileType php set errorformat=%m\ in\ %f\ on\ line\ %l
autocmd FileType c set omnifunc=ccomplete#Complete
syn on

augroup JumpCursorOnEdit
 au!
 autocmd BufReadPost *
 \ if expand("<afile>:p:h") !=? $TEMP |
 \ if line("'\"") > 1 && line("'\"") <= line("$") |
 \ let JumpCursorOnEdit_foo = line("'\"") |
 \ let b:doopenfold = 1 |
 \ if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
 \ let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
 \ let b:doopenfold = 2 |
 \ endif |
 \ exe JumpCursorOnEdit_foo |
 \ endif |
 \ endif
 " Need to postpone using "zv" until after reading the modelines.
 autocmd BufWinEnter *
 \ if exists("b:doopenfold") |
 \ exe "normal zv" |
 \ if(b:doopenfold > 1) |
 \ exe "+".1 |
 \ endif |
 \ unlet b:doopenfold |
 \ endif
augroup END

" setup php documenting
inoremap <C-P> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-P> :call PhpDocSingle()<CR>
vnoremap <C-P> :call PhpDocRange()<CR> 

set statusline=%f%m%r%h%w\ FMT=%{&ff}\ TP=%Y\ ASC=\%03.3b\ HEX=\%02.2B\ POS=%04l,%04v\ %p%%\ LEN=%L
set laststatus=2

" functions I wrote
python << EOF
import vim
import time
def Toggle(item):
    paste = vim.eval("&%s" % item)
    if vim.eval("&%s" % item) == "1":
        print "set no%s" % item
        vim.command("set no%s" % item)
    else:
        print "set %s" % item
        vim.command("set %s" % item)

def InsertLine(line):
    """
    Inserts line and puts the cursor
    at the end of it
    """
    w = vim.current.window
    cursor = w.cursor
    b = w.buffer
    b[cursor[0]:0] = [line]

def InsertDate(include_time=0):
    w = vim.current.window
    cursor = w.cursor
    b = w.buffer
    if include_time:
        format = "%a %Y-%m-%d %H:%M:%S"
    else:
        format = "%a %Y-%m-%d"
    b[cursor[0]-1:0] = [time.strftime(format)]

EOF

" mappings
nmap <F7> <Esc>:python Toggle("paste")<CR>
nmap <F8> <Esc>:python Toggle("hls")<CR>
nmap <F9> <Esc>:python InsertDate(include_time=1)<CR>

" abbreviations
iab plc ##########   ########## <Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>
iab prun <Esc>:r $HOME/.vim/templates/prun.tmpl<CR>i

