# python program to automate the process of
# checking in and checking out of my timesheet
# Future features may include reports
# or even replacing this script with a 
# web interface.

try: 
  from xml.etree import ElementTree
except ImportError:  
  from elementtree import ElementTree
import atom
import atom.service
import gdata.service
import gdata.spreadsheet
import gdata.spreadsheet.service
import string
import time
import sys
import datetime
import googledocs_timesheet_conf as c

class GoogleDocsTimesheet():
    
    def __init__(self):
        self.gd_client = self._login();
        self.wbid = self._get_workbook()
        self.wsid = self._get_worksheet()
        self.cur_date = self._get_current_date()
        self.cur_time = self._get_current_time()

    def _spreadsheet_query(self, sq):
        """
        not currently returning as I expect
        so I'm currently not using it
        """
        q = gdata.spreadsheet.service.ListQuery()
        q.reverse = 'true'
        q.sq = sq
        feed = self.gd_client.GetListFeed(self.wbid, self.wsid, query=q)
        return feed

    def _PrintFeed(self, feed):
        for i, entry in enumerate(feed.entry):
          if isinstance(feed, gdata.spreadsheet.SpreadsheetsCellsFeed):
            print '%s %s\n' % (entry.title.text, entry.content.text)
          elif isinstance(feed, gdata.spreadsheet.SpreadsheetsListFeed):
            print '%s %s %s\n' % (i, entry.title.text, entry.content.text)
          else:
            print '%s %s\n' % (i, entry.title.text)

    def _get_workbook(self):
        """
        This can be enhanced to query directly for the 
        spreadsheet we need by passing in the query parameter
        to the GetSpreadsheetsFeed call, but it is 
        currently not necessary as the number of spreadsheets 
        is small.
        """
        feed = self.gd_client.GetSpreadsheetsFeed()
        for wb in feed.entry:
            if wb.title.text == c.spreadsheet_name:
                timesheetid = wb.id.text.rsplit('/', 1)[1]
                break
        return timesheetid

    def _get_worksheet(self):
        feed = self.gd_client.GetWorksheetsFeed(self.wbid)
        worksheetid = feed.entry[string.atoi('0')].id.text.rsplit('/', 1)[1]
        return worksheetid
    
    def _login(self):
        gd_client = gdata.spreadsheet.service.SpreadsheetsService()
        gd_client.email = c.login
        gd_client.password = c.password
        gd_client.source = c.source
        try:
            gd_client.ProgrammaticLogin()
        except socket.sslerror:
            pass # this is a known error, but not a problem
            # see http://mail.python.org/pipermail/python-list/2005-August/338280.html
        return gd_client

    def _make_google_date(self, datestr):
        """
        mm/dd/yyyy to m/d/yyyy
        """
        month, day, year = datestr.split("/")
        if month[0] == '0':
            month = month[1]
        if day[0] == '0':
            day = day[1]
        return "/".join([month, day, year])

    def _get_current_date(self):
        d = time.strftime("%m/%d/%Y")
        gdate = self._make_google_date(d)
        return gdate

    def _get_current_time(self):
        return time.strftime("%H:%M:%S")
    
    def _timestr_to_timedelta(self, timestr):
        hours, minutes, seconds = [int(item) for item in timestr.split(":")]
        rdt = datetime.timedelta(hours=hours, minutes=minutes, seconds=seconds)
        return rdt
    
    def _calc_hours_worked_today(self):
        hwt = datetime.timedelta()
        for row in self.feed.entry:
            clockin = row.custom['clockin'].text
            clockout = row.custom['clockout'].text
            if clockout is None:
                clockout = self._get_current_time() 
            clockin = self._timestr_to_timedelta(clockin)
            clockout = self._timestr_to_timedelta(clockout)
            hwt += clockout - clockin
        return hwt 
    
    def _print_hours_worked_today(self):
        hwt = self._calc_hours_worked_today()
        seconds = hwt.seconds
        minutes = seconds/60
        hours = minutes/60
        minutes_left_over = minutes%60
        print "%s hours and %s minutes worked today." % (hours, minutes_left_over)

    def clock_in(self, punch_time):
        """
        clocks in
        """
        print "Clocking in..."
        # add a row
        row = {}
        row['date'] = self.cur_date
        row['clockin'] = punch_time
        result = self.gd_client.InsertRow(row, self.wbid, self.wsid)
        if isinstance(result, gdata.spreadsheet.SpreadsheetsList):
            print "Clocked in at %s %s" % (self.cur_date, punch_time)
            self._print_hours_worked_today()
        else:
            print "Problem clocking in"
        
    def clock_out(self, punch_time):
        """
        clocks out
        """
        print "Clocking out..."
        row = {}
        row['date'] = self.feed.entry[0].custom['date'].text
        row['clockin'] = self.feed.entry[0].custom['clockin'].text
        row['clockout'] = punch_time
        result = self.gd_client.UpdateRow(self.feed.entry[0], row)
        if isinstance(result, gdata.spreadsheet.SpreadsheetsList):
            print "Clocked out at %s %s" % (self.cur_date, punch_time)
            self._print_hours_worked_today()
        else:
            print "Problem clocking out"
    
    def _is_clocked_in(self):
        "returns true if clocked in"
        query = 'date=%s' % self.cur_date
        self.feed = self._spreadsheet_query(query)
        clocked_in = False
        for row in self.feed.entry:
            if row.custom['clockout'].text is None:
                clocked_in = True 
                break
        return clocked_in

    def toggle_login(self, punch_time=False):
        if not punch_time:
            punch_time = self.cur_time
        if (self._is_clocked_in()):
            self.clock_out(punch_time)
        else:
            self.clock_in(punch_time)

    def main(self, args):
        if len(args) > 1 and args[1] == "hours":
                if self._is_clocked_in():
                    print "Clocked in"
                else:
                    print "Clocked out"
                self._print_hours_worked_today()
        elif len(args) > 1 and args[1].find(":") != -1: # a time
            self.toggle_login(args[1])
        else:
            self.toggle_login()
        # stop the windows command prompt from going away immediately
        time.sleep(3)

if __name__ == "__main__":
    a = GoogleDocsTimesheet()
    a.main(sys.argv)
