#!/usr/bin/python

import sys
import datetime

start_time = sys.argv[1]
end_time = sys.argv[2]

start_hours, start_minutes = [int(item) for item in start_time.split(":")]
start = datetime.timedelta(hours=start_hours, minutes=start_minutes)
end_hours, end_minutes = [int(item) for item in end_time.split(":")]
end = datetime.timedelta(hours=end_hours, minutes=end_minutes)

time_worked = end-start
minutes_worked = time_worked.seconds/60.0

print "%.2f hours worked" % (time_worked.seconds/3600.0,)
print "%.2f minutes worked" % (minutes_worked,)
print "%d hours and %d minutes worked" % (minutes_worked/60, minutes_worked%60)
