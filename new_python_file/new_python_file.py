#!/usr/bin/python
# 
# Author: Paul D. Eden <paul@benchline.org>
# Distributed under the terms of the GNU Public License version 2
# Created: Mon Aug 4, 2008

import os
import sys
import datetime
import subprocess
from string import Template

TEMPLATE_DIR = "bin/misc/new_python_file"

def run(cmd):
    subprocess.check_call(cmd, shell=True)

def runbool(cmd):
    if subprocess.call(cmd, shell=True) == 0:
        return True
    else:
        return False

class NewPythonFile(object):
    """
    This class automates the creation of other new python files
    according to a pre-defined template.

    It also allows for the creation of skeleton test cases
    so that creating unit tests is less time consuming.
    """
    def _make_class_name_from_file_name(self, file_name):
        class_name = os.path.basename(file_name.replace(".py","").replace("_"," ").title().replace(" ",""))
        return class_name

    def _get_template_path(self, template):
        return "%s/%s/%s" % (os.environ["HOME"], TEMPLATE_DIR, template)

    def _ask_to_overwrite(self, new_file_name):
        if os.path.exists(new_file_name):
            ans = raw_input("Overwrite %s? " % new_file_name)
            if ans.lower() != "y":
                print "Not overwriting.  %s untouched" % new_file_name
                sys.exit(0)

    def _write_new_file(self, new_file_name, template):
        class_name = self._make_class_name_from_file_name(new_file_name)
        t = Template(open(self._get_template_path(template), "r").read())
        d = dict(class_name=class_name, date=datetime.date.today().strftime("%Y-%m-%d"))
        fo = open(new_file_name, "w")
        fo.write(t.substitute(d))
        fo.close()

    

    def write_tests_for(self, file_name):
        """
        Writes test skeletons for file_name
        """

    def make_new_python_file(self, new_file_name, template, noedit, overwrite):
        new_file_name = os.path.realpath(new_file_name)
        if not overwrite:
            self._ask_to_overwrite(new_file_name)
        self._write_new_file(new_file_name, template)
        print "Wrote %s using %s as a template" % (new_file_name, template)
        if not noedit:
            runbool("vim %s" % new_file_name)

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-t", "--template", help="template file to use [new_python_file.tmpl]",
                      default="new_python_file.tmpl")
    parser.add_option("-w", "--write-tests-for", help="write tests for")
    parser.add_option("-n", "--noedit", help="do not edit after create [False]", default=False, action="store_true")
    parser.add_option("-o", "--overwrite", help="overwrite existing files without prompting [False]", default=False, action="store_true")
    (options, args) = parser.parse_args()
    if options.write_tests_for:
        n = NewPythonFile()
        n.write_tests_for(options.write_tests_for)
    else:
        if len(args) < 1:
            parser.error("Please specify the filename of the file to create.")
        n = NewPythonFile()
        n.make_new_python_file(args[0], options.template,
                               options.noedit, options.overwrite)

