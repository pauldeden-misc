import subprocess
import mirror_traveldrive_conf as c

ret_value = subprocess.call("""/usr/bin/rsync -avP --del %s %s""" % (c.mounted_dir, c.copy_to_dir), shell=True)
