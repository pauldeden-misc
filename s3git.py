#!/usr/bin/python
# 
# Author: Paul D. Eden <paul@benchline.org>
# Distributed under the terms of the GNU Public License version 2
# Created: 2008-08-04

import os
import sys
import subprocess

def run(cmd):
    subprocess.check_call(cmd, shell=True)

def runbool(cmd):
    if subprocess.call(cmd, shell=True) == 0:
        return True
    else:
        return False

def output(cmd):
     process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
     rval = process.communicate()[0]
     if process.returncode != 0:
         raise Exception, "%s failed" % cmd
     return rval

class NotRepoException(Exception): pass

class S3Git(object):
    """
    Simple script to automate syncing a local git repository
    with Amazon S3

    The remote local git directory must have the same
    name as the remote S3 directory
    """
    def __init__(self):
        home = os.environ["HOME"]
        self.git_repo_dir = self._get_repo()
        self.shadow_repo_dir = self._setup_shadow_repo()

    def _get_repo(self, directory=""):
        """
        Recursively tries to find the git repo we are in
        if any
        """
        if not directory:
            directory = os.path.realpath(os.getcwd())
        if directory == "/":
            raise NotRepoException, "This directory is not a git repository"
        else:
            if self._is_git_repo(directory):
                return directory
            else:
                return self._get_repo(os.path.dirname(directory))

    def _setup_shadow_repo(self):
        """
        Makes the repo a clone of shadow_dir
        if necessary
        """
        short_name = os.path.basename(self.git_repo_dir)
        parent_dir = os.path.dirname(self.git_repo_dir)
        shadow_dir = os.path.join(parent_dir, ".%s.s3" % short_name)
        if not os.path.exists(shadow_dir):
            self._setup_clone(shadow_dir)
        return shadow_dir

    def _setup_clone(self, shadow_dir):
        # make sure this code is right.  It can be destructive
        os.chdir("/tmp")
        self._backup_repo()
        run("cp -a %s %s" % (self.git_repo_dir, shadow_dir))
        run("rm -fr %s" % self.git_repo_dir)
        run("git clone file://%s %s" % (shadow_dir, self.git_repo_dir))
        os.chdir(self.git_repo_dir)

    def _backup_repo(self):
        backup_dir = self.git_repo_dir + ".bak"
        if os.path.exists(backup_dir):
            run("rm -fr %s" % backup_dir)
        run("cp -a %s %s" % (self.git_repo_dir, backup_dir))

    def _is_git_repo(self, directory):
        return os.path.exists("%s/.git" % directory)

    def _commit_repo(self):
        runbool("git status")
        ans = raw_input("Commit the changes to git? ")
        if ans.lower() == "y":
            print "Here's a shell with which to commit."
            run("/bin/dash")

    def _s3_to_shadow(self):
        run("s3cmd --force --delete-removed sync s3://git_repos/%s %s" % (os.path.basename(self.git_repo_dir), self.shadow_repo_dir))

    def _shadow_to_s3(self):
        run("s3cmd --delete-removed sync %s s3://git_repos/%s" % (self.shadow_repo_dir,
            os.path.basename(self.git_repo_dir)))

    def pull(self):
        self._s3_to_shadow()
        run("git pull")

    def push(self):
        self._commit_repo()
        self._s3_to_shadow()
        run("git push")
        self._shadow_to_s3()

    def main(self, args):
        cmd = args[0]
        if cmd == "push":
            self.push()
        elif cmd == "pull":
            self.pull()
        else:
            raise Exception, "%s is an invalid command" % cmd

if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-v", "--verbose", action="store_true",
                      help="run gregariously")
    parser.add_option("-q", "--quiet", action="store_true",
                      help="run quietly")
    (options, args) = parser.parse_args()
    if len(args) < 1 or (args[0] not in ("push", "pull")):
        parser.error("You must specify one of 'push' or 'pull'")
    c = S3Git()
    c.main(args)
