#!/usr/bin/python
# 
# This program was born out of a need I have to stay focused while working.
# Many inconsequential emails were interrupting my mindset.
# 
# So, I created this script to remind me to check my email.  It is setup
# to be run from cron (or task scheduler if you are on windows) and show
# a little message box when you want to remind yourself to check your email.
# Use a line like the following in your cron settings to schedule (use your
# own schedule!)
# 05 8,10,12,14,15 * * mon-thu DISPLAY=:0 /fullpathto/email_reminder.py
# make sure you give teh email_reminder.py file execute permissions or 
# you run it explicitely through the python interpreter like 
# /usr/bin/python /fullpathto/email_reminder.py
# 
# Easygui <http://www.ferg.org/easygui> makes this trivial!
# 
# Copyright: Paul D. Eden <paul@benchline.org>
# License: GPL version 3 http://www.gnu.org/licenses/gpl.html
# 

from easygui import *

msgbox("Time to check your email. :-)")
