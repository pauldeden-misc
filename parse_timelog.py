import sys
import datetime

timelog_filename = sys.argv[1]
hourly_rate = float(sys.argv[2])

fo = open(timelog_filename)
lines = fo.readlines()
lines = [item.replace("\n", '') for item in lines]
outlines = []
for line in lines:
    if line:
        outlines.append(line)
del lines

total = datetime.timedelta()
in_working = False
start_dt = None
num_dates = 0
for line in outlines:
    try:
        dt = datetime.datetime.strptime(line, "%a %b %d %H:%M:%S %Z %Y")
        # after here a correctly formatted date is found
        num_dates += 1
        if in_working: # found the end datetime
            in_working = False
            # calc diff and add to total
	    punch = dt - start_dt
            print "one punch for %.2f hours" % (punch.seconds/3600.0,)
            total += punch
        else: # found the start datetime
            in_working = True
            # save start_dt
            start_dt = dt
    except ValueError:
        pass

if (num_dates%2) != 0:
    raise Exception, "Malformed file:  There needs to be an even number of datetime entries in the file to generate hours worked."

# now we should have the total time worked.  Go ahead and print it in hours.
hours_worked = total.seconds/3600.0
print "%.4f hours worked" % (hours_worked,)
invoice_amount = hours_worked * hourly_rate
print "At $%s an hour this makes a total of $%.2f to invoice" % (hourly_rate, invoice_amount)
